---
title: It's clean design makes it the most versatile.
image: /img/features/2-cleanest-design.jpg
image_alt: Cleanest design
---
We removed any unnecessary cuts and decorations to make the cleanest design possible.
Because of it this jacket goes well with any look.

Try it with distressed jeans and a t-shirt for a rugged style or with trousers and
a crisp white shirt for a refined look.