---
title: Breathable and stylish lining.
image: /img/features/lining.jpg
image_alt: Breathable and stylish lining
---
Lined in a smooth and breathable fabric for easy layering and comfort.