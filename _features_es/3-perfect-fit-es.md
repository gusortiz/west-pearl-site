---
title: Ajuste perfecto.
image: /img/features/perfect-fit.jpg
image_alt: Ajuste perfecto
---

El ajuste es la regla de oro del estilo y por lo tanto invertimos un año en desarrollar una prenda con el ajuste perfecto.
