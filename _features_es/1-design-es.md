---
title: Su diseño limpio la hace la más versátil.
image: /img/features/2-cleanest-design.jpg
image_alt: El Diseño más limpio
---
Removimos cortes no indispensables para su armado y decoraciones estorbosas, esto nos dió el diseño más limpio posible.
Por esta razón esta chaqueta le va a culaquier atuendo.

Pruebala con vaqueros desgastados y una camiseta para un estilo áspero o con pantalones de vestir y una camisa blanca para
un estilo refinado.