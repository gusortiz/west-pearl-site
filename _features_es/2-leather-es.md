---
title: Hecha a mano con piel de borrego de la más alta calidad.
image: /img/features/1-premium-leather.jpg
image_alt: Piel de la más alta calidad
---

Elaboradas con la más fina piel de borrego para un tacto suave, comodidad y una elegante caida sobre tu figura.

Esta chaqueta se siente bien al tocarla y tiene un aroma a piel irresistible.